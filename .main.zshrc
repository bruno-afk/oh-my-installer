#* ZSHRC File Template
# !-------------------------------------------------------------------
# ! User Variables
# !-------------------------------------------------------------------
# List of general plugins
    general_plugins=(
        common-aliases
        encode64
        sudo
        colored-man-pages
        zsh-autosuggestions # https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
        zsh-syntax-highlighting # https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md
    )
# !-------------------------------------------------------------------
# ! Import specific device setup
# !-------------------------------------------------------------------
export ZSH="$HOME/.oh-my-zsh"

if [ -f ~/.setup.zshrc ]; then
    source ~/.setup.zshrc
fi

plugins=(
    $general_plugins
    $custom_plugins
)


source $ZSH/oh-my-zsh.sh


# !-------------------------------------------------------------------
# ! Load main part of zshrc file
# !-------------------------------------------------------------------

# /-------------------------------------------------------------------
# / Plugins
# /-------------------------------------------------------------------


# /-------------------------------------------------------------------
# / Source & etc
# /-------------------------------------------------------------------

#***********
# Support for ssh utf
#***********
export LANG=en_US.UTF-8

COMPLETION_WAITING_DOTS="true"

#***********
# SSH
#***********
export SSH_KEY_PATH="~/.ssh"

# /-------------------------------------------------------------------
# / Alias OS Specific
# /--------------------------
# Basics
alias c="clear"
alias zshrc='$editor ~/.zshrc'
alias zsh-setup='$editor ~/.setup.zshrc'
alias zsh-device='$editor ~/.device.zshrc'

# /-------------------------------------------------------------------
# / Other stuff
# /-------------------------------------------------------------------

#**************
# Show weather
#**************
weather() {
    if [ "$1" != "" ] 
    then
        curl Wttr.in/$1
    else
        curl Wttr.in
    fi
}

#*************************************
# Git - automatic git commit and push
#*************************************
gitall() {
    git add --all
    if [ "$1" != "" ]; then
        git commit -m "$1"
    else
        git commit -m "QuickFixTM"
    fi
    git push
}
git-again() {
    git add --all
    git commit --amend --no-edit
    git push
}

git-all-pull() {
    find . -maxdepth 3 -name .git -type d | rev | cut -c 6- | rev | xargs -I {} git -C {} pull
}

git-all-push() {
    find . -maxdepth 3 -name .git -type d | rev | cut -c 6- | rev | xargs -I {} gitall
}

#*************************************
# EXTRACT SCRIPT
#*************************************
function extract() {
    echo Extracting $1 ...
    if [ -f $1 ]; then
        case $1 in
        *.tar.bz2) tar xjf $1 ;;
        *.tar.gz) tar xzf $1 ;;
        *.bz2) bunzip2 $1 ;;
        *.rar) unrar x $1 ;;
        *.gz) gunzip $1 ;;
        *.tar) tar xf $1 ;;
        *.tbz2) tar xjf $1 ;;
        *.tgz) tar xzf $1 ;;
        *.zip) unzip $1 ;;
        *.Z) uncompress $1 ;;
        *.7z) 7z x $1 ;;
        *) echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# /-------------------------------------------------------------------
# / Import specific device exprots and alias
# /-------------------------------------------------------------------
if [ -f ~/.device.zshrc ]; then
    source ~/.device.zshrc
fi

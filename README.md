# Oh my ZSH installer

- Install ZSH
- Install Oh My ZSH
- Install basic plugin pack
- Install basic functions
- Instal basic script or custom script
- Custom script detect your apps and install plugins for them

**Normal install:**
```
bash <(curl -s https://gitlab.com/bruno-afk/oh-my-installer/-/raw/main/helper.sh) -c
```


**Custom install for local device:**

```
bash <(curl -s https://gitlab.com/bruno-afk/oh-my-installer/-/raw/main/helper.sh) -c local
```

**Custom install for server env:**

```
bash <(curl -s https://gitlab.com/bruno-afk/oh-my-installer/-/raw/main/helper.sh) -c server
```

#!/bin/bash
#* zsh -- Oh my zsh -- custom zshrc install

#?++++++++++++++++++++++++++
#? Main variables
#?++++++++++++++++++++++++++

# Folders and files
script_folder_name="oh-my-installer-main"

# Color setup
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
LIME_YELLOW=$(tput setaf 190)
POWDER_BLUE=$(tput setaf 153)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
BRIGHT=$(tput bold)
NORMAL=$(tput sgr0)
BLINK=$(tput blink)
REVERSE=$(tput smso)
UNDERLINE=$(tput smul)

#?++++++++++++++++++++++++++
#? Main arrays
#?++++++++++++++++++++++++++

system_list=("Linux" "Darwin") # List of systems to check

# List of apps that we can check with which
app_list=("brew" "git" "zsh" "docker-compose" "netlify" "nginx" "mysql" "php" "apt" "yum" "python" "docker" "node" "terraform" "ansible" "wget" "tilix" "wsl.exe" "code" "unzip" "zip" "tlp" "tlp-stat" "vim" "ufw" "vagrant" "vault" "sudo" "rsync" "pm2" "pip" "node" "kubectl" "iterm2" "httpie" "flutter" "doctl" "aws" "wp" "dnf" "neofetch" "pavucontrol" "batcat" "chsh")

# List of scripts - script name
dir_list_name=("nvm" "oh-my-zsh")

# List of scripts - script path
dir_list_path=(
    ~/.nvm
    ~/.oh-my-zsh
)

#?++++++++++++++++++++++++++
#? Check loops
#?++++++++++++++++++++++++++

# Sys section check
sys_count=0
for sys in "${system_list[@]}"
do
    if [[ $(uname) == "$sys" ]]; then
        var=$(echo "has_${sys//-/}")
        declare "$var=1"
    fi
    sys_count=$(( $sys_count + 1 ))
done

# App section check
app_count=0
for apps in "${app_list[@]}"
do
    if [[ -x $(which $apps) ]]; then
        var=$(echo "has_${apps//-/}")
        master_name=$(echo "${var//./}")
        declare "$master_name=1"
    fi
    app_count=$(( $app_count + 1 ))
done 3>&1 &>/dev/null

# Script section check
dir_count=0
for dir_a in "${dir_list_name[@]}"
do
    if [[ -d ${dir_list_path[$dir_count]} ]]; then
        var=$(echo "has_${dir_a//-/}")
        declare "$var=1"
    fi
    dir_count=$(( $dir_count + 1 ))
done

#?++++++++++++++++++++++++++
#? Check writer loops
#?++++++++++++++++++++++++++

function writer_check() {
    # Printf setup
    divider===============================
    divider=$divider$divider
    header="\n %-20s  %-20s\n"
    format=" %-20s  %-20s\n"
    printf "$header" "App name" "Status"
    width=43
    printf "%$width.${width}s\n" "$divider"
    
    # Write all systems
    check_sys_count=0
    for on_sys in "${system_list[@]}"
    do
        name=$(echo "${on_sys//-/}")
        declare "var=has_$name"
        #echo "Is $on_sys active: ${!var}" | sed 's/\t/,|,/g'
        if [[ ${!var} == 1 ]]; then
            status="${GREEN}Installed${NORMAL}"
        else
            status="${RED}Not Installed${NORMAL}"
        fi
        printf "$format" "$on_sys" "$status"
        check_sys_count=$(( $check_sys_count + 1 ))
    done
    
    # Write all apps
    check_app_count=0
    for on_apps in "${app_list[@]}"
    do
        name=$(echo "${on_apps//-/}")
        master_name=$(echo "${name//./}")
        declare "var=has_$master_name"
        #echo "Is $on_apps active:|${!var}"  | sed 's/\t/,|,/g' | column -s ',' -t
        if [[ ${!var} == 1 ]]; then
            status="${GREEN}Installed${NORMAL}"
        else
            status="${RED}Not Installed${NORMAL}"
        fi
        printf "$format" "$on_apps" "$status"
        check_app_count=$(( $check_app_count + 1 ))
    done
    
    # Write all scripts
    check_dir_count=0
    for on_dir in "${dir_list_name[@]}"
    do
        name=$(echo "${on_dir//-/}")
        declare "var=has_$name"
        #echo "Is $on_dir active:| ${!var}" | sed 's/\t/,|,/g'
        if [[ ${!var} == 1 ]]; then
            status="${GREEN}Installed${NORMAL}"
        else
            status="${RED}Not Installed${NORMAL}"
        fi
        printf "$format" "$on_dir" "$status"
        check_dir_count=$(( $check_dir_count + 1 ))
    done
}
#?++++++++++++++++++++++++++
#? Script start
#?++++++++++++++++++++++++++

#! Clear the screen
#clear
echo
echo
#! Run writer check
echo "${BLUE}List of system, apps and script installed${NORMAL}"
writer_check
echo
echo
echo

#?++++++++++++++++++++++++++
#? Dependency install
#?++++++++++++++++++++++++++

#! git check
if [[ -z $has_git ]]; then
    if [[ $has_apt == 1 ]]; then
        sudo apt install git -y
    elif [[ $has_dnf == 1 ]]; then
        sudo dnf install git -y
    else
        echo "You are not using APT system, we are for now supporting just systems with APT"
        echo
    fi
else
    echo "${BLUE}You have git installed${NORMAL}"
    echo
fi

#! ZSH check
if [[ -z $has_zsh ]]; then
    if [[ $has_apt == 1 ]]; then
        sudo apt install zsh -y
    elif [[ $has_dnf == 1 ]]; then
        sudo dnf install zsh -y
    else
        echo "You are not using APT system, we are for now supporting just systems with APT"
        echo
    fi
else
    echo "${BLUE}You have ZSH installed${NORMAL}"
    echo
fi
#! unzip check
if [[ -z $has_unzip ]]; then
    if [[ $has_apt == 1 ]]; then
        sudo apt install unzip -y
    elif [[ $has_dnf == 1 ]]; then
        sudo dnf install unzip -y
    else
        echo "You are not using APT system, we are for now supporting just systems with APT"
        echo
    fi
else
    echo "${BLUE}You have unzip installed${NORMAL}"
    echo
fi
#! chsh check
if [[ -z $has_chsh ]]; then
    if [[ $has_apt == 1 ]]; then
        sudo apt install util-linux-user-y
    elif [[ $has_dnf == 1 ]]; then
        sudo dnf install util-linux-user -y
    else
        echo "You are not using APT system, we are for now supporting just systems with APT"
        echo
    fi
else
    echo "${BLUE}You have unzip installed${NORMAL}"
    echo
fi
#! Oh my zsh
if [[ -z $has_ohmyzsh ]]; then
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended
    ZSH_CUSTOM="$HOME/.oh-my-zsh/custom"
    echo
else
    ZSH_CUSTOM="$HOME/.oh-my-zsh/custom"
    echo "${BLUE}You have ohmyzsh installed${NORMAL}"
    echo
fi

#! Plugins install
if [[ ! -d "$ZSH_CUSTOM/plugins/zsh-syntax-highlighting" ]]; then
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
fi

if [[ ! -d "$ZSH_CUSTOM/plugins/zsh-autosuggestions" ]]; then
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions

fi

#?++++++++++++++++++++++++++
#? Temp create and clear
#?++++++++++++++++++++++++++

function temp_creator() {
    tmp_dir=$(mktemp -d -t ci-XXXXXXXXXX)
}

function temp_clear() {
    rm -rf $tmp_dir
}

#?++++++++++++++++++++++++++
#? Scripts download
#?++++++++++++++++++++++++++

function classic_install() {
    if [[ -f $HOME/.zshrc ]]; then
        cp $HOME/.zshrc $HOME/.zshrc-old
    fi
    curl -o $HOME/.zshrc https://gitlab.com/bruno-afk/oh-my-installer/-/raw/main/.zshrc
    touch $HOME/.device.zsh
}

function modify_install() {
    curl -o $tmp_dir/zsh.zip https://gitlab.com/bruno-afk/oh-my-installer/-/archive/main/oh-my-installer-main.zip 3>&1 &>/dev/null
    unzip $tmp_dir/zsh.zip -d $tmp_dir 3>&1 &>/dev/null
    source_folder=$tmp_dir/$script_folder_name
}

#?++++++++++++++++++++++++++
#? Scripts modify
#?++++++++++++++++++++++++++

function modify_zsh() {
    # Create folders
    edit_folder=$tmp_dir/edit
    mkdir $edit_folder
    
    # Transfer needed files
    cp $source_folder/.main.zshrc $edit_folder/.zshrc
    touch $edit_folder/.setup.zshrc
    
    # Help function
    function transfer_values() {
        echo -e $1 >> $edit_folder/.setup.zshrc
    }
    # Script starter
    echo > $edit_folder/.setup.zshrc
    transfer_values "# This is custom setup script created by script"
    transfer_values
    
    # Env writer
    if [[ $ENV_SETUP == "local" ]]; then
        transfer_values "# You are using this script on your local machine"
        transfer_values "ENV_ZSH=local"
    else
        transfer_values "# You are using this script on your server"
        transfer_values "ENV_ZSH=server"
    fi
    
    # Theme setup
    if [[ $ENV_SETUP == "local" ]]; then
        if [[ ! -d "$ZSH_CUSTOM/themes/spaceship-prompt" ]]; then
            # Setup theme https://github.com/spaceship-prompt/spaceship-prompt
            git clone https://github.com/spaceship-prompt/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" --depth=1
            ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"
        fi
        # Enable theme
        transfer_values 'ZSH_THEME="spaceship" # Theme'
    else
        transfer_values 'ZSH_THEME="sporty_256" # Theme'
    fi
    
    # Editor setup
    if [[ $ENV_SETUP == "local" ]]; then
        if [[ $has_code -eq 1 ]]; then
            transfer_values 'editor=code # Default text editor'
        else
            transfer_values 'editor=nano # Default text editor'
        fi
        
    else
        if [[ $has_vim -eq 1 ]]; then
            transfer_values 'editor=vim # Default text editor'
        else
            transfer_values 'editor=nano # Default text editor'
        fi
    fi
    
    # Plugin setup
    transfer_values
    transfer_values "# Custom plugins"
    transfer_values 'custom_plugins=('
    acccept_app_count=0
    non_existing_plug=("zsh" "php" "pavucontrol" "mysql" "unzip" "zip" "tilix" "wget" "tlp" "tlp-stat" "neofetch" "nginx" "batcat" "wsl.exe" "chsh")
    for accepted in "${app_list[@]}"
    do
        name=$(echo "${accepted//-/}")
        master_name=$(echo "${name//./}")
        declare "var=has_$master_name"
        if [[ ${!var} -eq 1 ]]; then
            if [[ " ${non_existing_plug[*]} " =~ " ${accepted} " ]]; then
                sleep .1
                elif [[ $accepted == "apt" ]]; then
                transfer_values "ubuntu"
                elif [[ $accepted == "vim" ]]; then
                transfer_values "vim-interaction"
                elif [[ $accepted == "code" ]]; then
                transfer_values "vscode"
                elif [[ $accepted == "wp" ]]; then
                transfer_values "wp-cli"
            else
                transfer_values "$accepted"
            fi
        fi
        acccept_app_count=$(( $acccept_app_count + 1 ))
    done
    acccept_sys_count=0
    for accepted_sys in "${system_list[@]}"
    do
        name=$(echo "${accepted_sys//-/}")
        master_name=$(echo "${name//./}")
        declare "var=has_$master_name"
        if [[ ${!var} -eq 1 ]]; then
            if [[ $accepted_sys == "Darwin" ]]; then
                transfer_values "macos"
            fi
        fi
        acccept_sys_count=$(( $acccept_sys_count + 1 ))
    done
    acccept_script_count=0
    for accepted_script in "${dir_list_name[@]}"
    do
        name=$(echo "${accepted_script//-/}")
        master_name=$(echo "${name//./}")
        declare "var=has_$master_name"
        if [[ ${!var} -eq 1 ]]; then
            if [[ $accepted_script == "oh-my-zsh" ]]; then
                sleep .1
            else
                transfer_values "$accepted_script"
            fi
        fi
        acccept_script_count=$(( $acccept_script_count + 1 ))
    done
    transfer_values ')'
    
    # Add specific things
    if [[ $has_wsl -eq 1 ]]; then
        transfer_values "cd /home/$USER"
        transfer_values 'winUser=$(/mnt/c/Windows/System32/cmd.exe /c '"echo %USERNAME%"' | sed -e "s/\\r//g") &>/dev/null'
        transfer_values 'alias win="cd /mnt/c/Users/$winUser/"'
    elif [[ $has_Darwin -eq 1 ]]; then
        transfer_values "alias ql='qlmanage -p 2>/dev/null' # OS X Quick Look"
        transfer_values"alias oo='open .'                  # open current directory in OS X Finder"
        transfer_values "alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'"
        transfer_values "alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'"
        transfer_values "alias today='calendar -A 0 -f /usr/share/calendar/calendar.mark | sort'"
        transfer_values "alias mailsize='du -hs ~/Library/mail'"
        transfer_values "alias smart='diskutil info disk0 | grep SMART' # display SMART status of hard drive&"
        # Flush DNS
        transfer_values "alias dnsflush='sudo killall -HUP mDNSResponder; sleep 2; echo I just kill the DNS Cache | say'"
        
        # change hosts file
        transfer_values "alias hosts='sudo \$editor /etc/hosts'"
        transfer_values 'pman() {
            ps=$(mktemp -t manpageXXXX).ps
            man -t $@ >"$ps"
            open "$ps"
        }'
    elif [[ $has_Linux -eq 1 ]]; then
        transfer_values
        transfer_values "#Add linux speficic alias"
        transfer_values 'alias c="clear"'
        transfer_values 'alias hosts_edit="sudo $editor /etc/hosts"'
        transfer_values 'alias myip="curl -4 icanhazip.com"'
        transfer_values 'PATH=$PATH:/home/$USER/.local/bin'
        if [[ $has_pavucontrol -eq 1 ]]; then
            transfer_values 'alias sound-settings="pavucontrol"'
        fi
        if [[ $has_batcat -eq 1 ]]; then
            transfer_values 'alias cat="batcat"'
        fi
        if [[ $has_neofetch -eq 1 ]]; then
            transfer_values 'alias sexy="echo && echo && echo && echo && neofetch && echo && echo"'
        fi
        if [[ $has_nvm -eq 1 ]]; then
            transfer_values 'export NVM_DIR="$HOME/.nvm"'
            transfer_values '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm'
            transfer_values '[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion'
        fi
        if [[ $ENV_SETUP == "local" ]]; then
            if [[ $has_tilix -eq 1 ]]; then
                if [[ $has_dapt == 1 ]]; then
                    transfer_values '. /etc/profile.d/vte-2.91.sh '
                fi
            fi
            if [[ $has_tlp -eq 1 ]]; then
                transfer_values " # Battery setcharge tlp \n
                alias battery-normal='sudo tlp setcharge 95 100' \n
                alias battery-save='sudo tlp setcharge 75 80'"
            fi
            if [[ $has_tlpstat -eq 1 ]]; then
                transfer_values "alias stat-tlp='sudo tlp-stat -s && sudo tlp-stat -w'"
            fi
            if [[ $has_node -eq 1 ]]; then
                transfer_values "alias serve='npm install -g serve && serve'"
            fi
        else
            if [[ $has_nginx -eq 1 ]]; then
                transfer_values "
                alias ng-enable='cd /etc/nginx/sites-enabled/'\n
                alias ng-available='cd /etc/nginx/sites-available/'\n
                alias ng-start='sudo systemctl start nginx'\n
                alias ng-restart='sudo nginx -t && sudo systemctl restart nginx'
                alias ng-stop='sudo systemctl stop nginx'\n
                alias ng-log-error='vim /var/log/nginx/error.log;'\n
                alias ng-test='sudo nginx -t'"
            fi
        fi
    fi
}

#?++++++++++++++++++++++++++
#? Scripts install
#?++++++++++++++++++++++++++
function install_zshscripts() {
    cp -a $edit_folder/. $HOME/
    touch $HOME/.device.zshrc
}

#?++++++++++++++++++++++++++
#? ZSH Activator
#?++++++++++++++++++++++++++
function zsh_activator() {
    if [[ $SHELL =~ "zsh" ]]; then
        echo "You are using ZSH as default shell"
    else
        echo "Switch to zsh"
        chsh -s $(which zsh)
        exec zsh
    fi
}
#?++++++++++++++++++++++++++
#? Env checker
#?++++++++++++++++++++++++++
function custom_script() {
    temp_creator
    modify_install
    modify_zsh
    install_zshscripts
    zsh_activator
    temp_clear
}

while getopts ":c" option; do
    case $option in
        c)
            if [[ $2 == "server" ]]; then
                ENV_SETUP="server"
                custom_script
                exit 0
                elif [[ $2 == "local" ]]; then
                ENV_SETUP="local"
                custom_script
                exit 0
            else
                classic_install
                zsh_activator
                exit 0
            fi
        ;;
        \?) # Invalid option
            echo "Error: Invalid option"
        exit 1;;
    esac
done

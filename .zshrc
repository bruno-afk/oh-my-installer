#* ZSHRC File Template
# !-------------------------------------------------------------------
# ! User Variables
# !-------------------------------------------------------------------
# Set your ENV
    editor="vim"

# List of general plugins
    general_plugins=(
        common-aliases
        encode64
        git
        docker
        docker-compose
        sudo
        colored-man-pages
        npm
        httpie
        bundler
        pip
        nvm
        gem
    )
# Env specific setup
    # Theme 
    ZSH_THEME="sporty_256" #Ideal for server
    #ZSH_THEME="spaceship" # https://github.com/denysdovhan/spaceship-prompt
     # Plugins
    myplugins=(
        $general_plugins
        $my_plugins
        zsh-autosuggestions # https://github.com/zsh-users/zsh-autosuggestions/blob/master/INSTALL.md
        zsh-syntax-highlighting # https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md
        kubectl
        terraform
    )

# /-------------------------------------------------------------------
# / Load main part of zshrc file
# /-------------------------------------------------------------------
# !-------------------------------------------------------------------
# ! System Variables
# !-------------------------------------------------------------------
checkme () {
    if [[ $(uname) == 'Linux' ]]; then
        IS_LINUX=1
    fi

    if [[ $(uname) == 'Darwin' ]]; then
        IS_MAC=1
    fi

    if [[ -x $(which brew) ]]; then
        HAS_BREW=1
    fi
    if [[ -x $(which wsl.exe) ]]; then
        HAS_WSLE=1
    fi

    if [[ -x $(which apt-get) ]]; then
        HAS_APT=1
    fi

    if [[ -x $(which yum) ]]; then
        HAS_YUM=1
    fi
    if [[ -x $(which nginx) ]]; then
        HAS_NGINX=1
    fi
    if [[ -x $(which mysql) ]]; then
        HAS_MYSQL=1
    fi
    if [[ -x $(which php) ]]; then
        HAS_PHP=1
    fi
    if [[ -x $(which python) ]]; then
        HAS_PYTHON=1
    fi
    if [[ -x $(which docker) ]]; then
        HAS_DOCKER=1
    fi
    if [[ -x $(which node) ]]; then
        HAS_NODE=1
    fi
    if [[ -x $(which code) ]]; then
        HAS_CODE=1
    fi

}

checkme 3>&1 &>/dev/null

# /-------------------------------------------------------------------
# / OMZ location
# /-------------------------------------------------------------------

#******************
# Mac 
#******************
if [[ $IS_MAC -eq 1 ]]; then
    export ZSH="/Users/$USER/.oh-my-zsh"
    export DISPLAY=:0
fi

#******************
# Linux
#******************
if [[ $IS_LINUX -eq 1 ]]; then
    export ZSH="/home/$USER/.oh-my-zsh"
fi

# /-------------------------------------------------------------------
# / Plugins
# /-------------------------------------------------------------------

if [[ $IS_MAC -eq 1 ]]; then
    plugins=(
        cake
        osx
        $myplugins
   )
fi

if [[ $IS_LINUX -eq 1 ]]; then
    plugins=(
        ubuntu
        command-not-found
        compleat
        $myplugins
    )
fi 

# /-------------------------------------------------------------------
# / Source & etc
# /-------------------------------------------------------------------

source $ZSH/oh-my-zsh.sh

#***********
# Support for ssh utf
#***********
export LANG=en_US.UTF-8

COMPLETION_WAITING_DOTS="true"

#***********
# SSH
#***********
export SSH_KEY_PATH="~/.ssh"

# /-------------------------------------------------------------------
# / Alias OS Specific
# /-------------------------------------------------------------------
if [[ $HAS_WSLE -eq 1 ]]; then
    cd /home/$USER
    winUser=$(/mnt/c/Windows/System32/cmd.exe /c 'echo %USERNAME%' | sed -e 's/\r//g') &>/dev/null
    alias win="cd /mnt/c/Users/$winUser/"
fi

if [[ $IS_MAC -eq 1 ]]; then
    #***********
    # OS Alias section
    #***********
    alias ql='qlmanage -p 2>/dev/null' # OS X Quick Look
    alias oo='open .'                  # open current directory in OS X Finder
    alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'
    alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'
    alias 'today=calendar -A 0 -f /usr/share/calendar/calendar.mark | sort'
    alias 'mailsize=du -hs ~/Library/mail'
    alias 'smart=diskutil info disk0 | grep SMART' # display SMART status of hard drive&

    # alias to show all Mac App store apps
    alias apps='mdfind "kMDItemAppStoreHasReceipt=1"'

    # refresh brew by upgrading all outdated casks
    alias refreshbrew='brew outdated | while read cask; do brew upgrade $cask; done'

    # Flush DNS
    alias dnsflush="sudo killall -HUP mDNSResponder; sleep 2; echo I just kill the DNS Cache | say"

    # change hosts file
    alias hosts="sudo code /etc/hosts"

    # ZSHRC file edit in code
    if [[ $HAS_CODE -eq 1 ]]; then
        alias zshrc='$editor ~/.zshrc'
        alias zshalias='$editor ~/.zshalias'
        alias zshlocal='$editor ~/.zshlocal'
    else
        editor="nano"
        alias zshrc='$editor ~/.zshrc'
        alias zshalias='$editor ~/.zshalias'
        alias zshlocal='$editor ~/.zshlocal'
    fi

    #***********
    # view man pages in Preview
    #***********
    pman() {
        ps=$(mktemp -t manpageXXXX).ps
        man -t $@ >"$ps"
        open "$ps"
    }

    #***********
    # Show ip adress (internal and external)
    #***********
    function myip() {
        dig +short myip.opendns.com @resolver1.opendns.com | awk '{print "External IP address: " $1}'
        ifconfig lo0 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "lo0                : " $2}'
        ifconfig en0 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "en0 (IPv4)         : " $2 " " $3 " " $4 " " $5 " " $6}'
        ifconfig en0 | grep 'inet6 ' | sed -e 's/ / /' | awk '{print "en0 (IPv6)         : " $2 " " $3 " " $4 " " $5 " " $6}'
        ifconfig en1 | grep 'inet ' | sed -e 's/:/ /' | awk '{print "en1 (IPv4)          : " $2 " " $3 " " $4 " " $5 " " $6}'
        ifconfig en1 | grep 'inet6 ' | sed -e 's/ / /' | awk '{print "en1 (IPv6)         : " $2 " " $3 " " $4 " " $5 " " $6}'
    }

    #***********
    # Dev tools
    #***********

    # Visual studio code integration
    function code() {
        if [[ $# == 0 ]]; then
            open -a "Visual Studio Code"
        else
            local argPath="$1"
            [[ $1 == /* ]] && argPath="$1" || argPath="$PWD/${1#./}"
            open -a "Visual Studio Code" "$argPath"
        fi
    }

    # Serve
    if [[ $HAS_PYTHON3 -eq 1 ]]; then
        alias serve='python3 -m http.server'
    fi
    if [[ $HAS_NODE -eq 1 ]]; then
        alias serve-npm='npm install -g serve && serve'
    fi
fi

if [[ $IS_LINUX -eq 1 ]]; then

    # Basics
    alias c="clear"
    alias cc="sexy && echo && echo && echo && echo && echo && echo && echo && echo && echo && clear"

    # Hosts
    alias hosts_edit="sudo $editor /etc/hosts"

    # My IP address
    alias myip='curl -4 icanhazip.com'

    # Basic info
    alias sexy='echo && echo && echo && neofetch' #if nothing is shown you need to install neofetch
    alias stat-tlp='sudo tlp-stat -s && sudo tlp-stat -w'
    alias stat-battery='sudo tlp-stat -b && sudo tlp-stat -t && sudo tlp-stat -w'
    alias stat-cpu='lscpu | grep MHz && sudo tlp-stat -p'
    alias stat-watch-cpu='watch -d "cat /proc/cpuinfo | grep -i Mhz"'

    # Battery setcharge tlp
    alias battery-normal='sudo tlp setcharge 95 100'
    alias battery-save='sudo tlp setcharge 75 80'

    # ZSHRC file edit in code
    if [[ $HAS_CODE -eq 1 ]]; then
        alias zshrc='$editor ~/.zshrc'
        alias zshalias='$editor ~/.zshalias'
        alias zshlocal='$editor ~/.zshlocal'
    else
        editor="nano"
        alias zshrc='$editor ~/.zshrc'
        alias zshalias='$editor ~/.zshalias'
        alias zshlocal='$editor ~/.zshlocal'
    fi

    #***********
    # Dev and Ops tools
    #***********
    # Docker
    docker-publish() {
        set -euo pipefail
        docker build -t $1 .
        docker push $1
    }
    kube-service() {
        EXTERNAL_IP=""
        ORDER="$1"
        CONTEXT="$2"
        SERVICE="$3"
        if [[ $ORDER = "ip" ]]; then
            while [ -z $EXTERNAL_IP ]; do
            if [ $CONTEXT = "-d" ];then
            EXTERNAL_IP=$(kubectl get service ${SERVICE} --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}")
            else
            EXTERNAL_IP=$(kubectl --context ${CONTEXT} get service ${SERVICE} --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}")
            fi

            if [ -z "${EXTERNAL_IP}" ]; then
                echo "Waiting for external IP..."
                sleep 10
            fi
            done
            echo "Ready: ${EXTERNAL_IP}"
        else 
            echo "
            You can use:
            kube-service ip CLSUTER_NAME SERVICE_NAME
            or
            kube-service ip -d SERVICE_NAME
            "
        fi
    }
    # MySql
    if [[ $HAS_MYSQL -eq 1 ]]; then
        alias mysql='mysql -u root'
        alias mysqladmin='mysqladmin -u root'
    fi

    # NGINX
    if [[ $HAS_NGINX -eq 1 ]]; then
        alias ng-sites-enable='cd /etc/nginx/sites-enabled/'
        alias ng-sites-available='cd /etc/nginx/sites-available/'
        alias ng-start='sudo systemctl start nginx'
        alias ng-stop='sudo systemctl stop nginx'
        asource ~/.zshmainlias ng-log-access='vim /var/log/nginx/access.log;'
        alias ng-log-error='vim /var/log/nginx/error.log;'
        alias ng-test='sudo nginx -t'
    fi

    # Apache 2
    if [[ $HAS_APACHE2 -eq 1 ]]; then
        alias ap2start='sudo service apache2 start'
        alias ap2restart='sudo service apache2 restart'
        alias ap2stop='sudo service apache2 stop'
    fi

    # PHP
    if [[ $HAS_APACHE2 -eq 1 ]]; then
        alias php-restart='sudo systemctl restart php7.0-fpm'
    fi

    # Serve
    if [[ $HAS_PYTHON3 -eq 1 ]]; then
        alias host='python3 -m http.server'
    fi
    if [[ $HAS_NODE -eq 1 ]]; then
        alias serve='npm install -g serve && serve'
    fi
fi


# /-------------------------------------------------------------------
# / Other stuff
# /-------------------------------------------------------------------

#**************
# Show weather
#**************
weather() {
    if [ "$1" != "" ] 
    then
        curl Wttr.in/$1
    else
        curl Wttr.in
    fi
}

#**************
# Tilix stuff
#**************

if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
        source /etc/profile.d/vte.sh
fi

#*************************************
# Git - automatic git commit and push
#*************************************
gitall() {
    git add --all
    if [ "$1" != "" ]; then
        git commit -m "$1"
    else
        git commit -m "QuickFixTM"
    fi
    git push
}
git-again() {
    git add --all
    git commit --amend --no-edit
    git push
}

git-all-pull() {
    find . -maxdepth 3 -name .git -type d | rev | cut -c 6- | rev | xargs -I {} git -C {} pull
}

git-all-push() {
    find . -maxdepth 3 -name .git -type d | rev | cut -c 6- | rev | xargs -I {} gitall
}

#*************************************
# EXTRACT SCRIPT
#*************************************
function extract() {
    echo Extracting $1 ...
    if [ -f $1 ]; then
        case $1 in
        *.tar.bz2) tar xjf $1 ;;
        *.tar.gz) tar xzf $1 ;;
        *.bz2) bunzip2 $1 ;;
        *.rar) unrar x $1 ;;
        *.gz) gunzip $1 ;;
        *.tar) tar xf $1 ;;
        *.tbz2) tar xjf $1 ;;
        *.tgz) tar xzf $1 ;;
        *.zip) unzip $1 ;;
        *.Z) uncompress $1 ;;
        *.7z) 7z x $1 ;;
        *) echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

# /-------------------------------------------------------------------
# / Import specific device alias
# /-------------------------------------------------------------------
if [ -f ~/.device.zsh ]; then
    source ~/.device.zsh
fi
